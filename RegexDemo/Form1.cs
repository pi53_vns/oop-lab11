﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace RegexDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }        

        private void Form1_Load(object sender, EventArgs e)
        {
            labelPhoneNumber.Visible = false;
            labelPassport.Visible = false;
            labelNumber.Visible = false;
            labelName.Visible = false;
            labelTime.Visible = false;
            labelMail.Visible = false;
        }

        private void checkPhoneNumber_Click(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"(((\+?3)?8)?0((50)|(39)|9[0-9]|7[03]|6[36-8]))\d{7}\b");
            Match match = regex.Match(textBoxPhoneNumber.ToString());
            labelPhoneNumber.Visible = true;
            if (match.Success == true)
            {
                labelPhoneNumber.ForeColor = Color.Green;
                labelPhoneNumber.Text = "OK";
            }
            else
            {
                labelPhoneNumber.ForeColor = Color.Red;
                labelPhoneNumber.Text = "Помилка";
            }
        }

        private void checkPassport_Click(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"(А[АВТСЮЕКМ]|В[АВЕНСТО]|Е[АВСКМН]|М[АЕСКНО]|К[АВСЕКМНР]|C[АВСЕЮЛНИКМТР]|ТТ|Н[АЕСК])\d{6}\b");
            Match match = regex.Match(textBoxPassport.ToString());
            labelPassport.Visible = true;
            if (match.Success == true)
            {
                labelPassport.ForeColor = Color.Green;
                labelPassport.Text = "OK";                             
            }
            else
            {
                labelPassport.ForeColor = Color.Red;
                labelPassport.Text = "Помилка";
            }
        }

        private void checkNumber_Click(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"\b((1031[1-9])|(103[2-9]\d)|(10[4-9]\d{2})|(1[1-9]\d{3})|([2-7]\d{4})|(8[0-8]\d{3})|(89[0-6][0-4][0-5]))\b");
            Match match = regex.Match(textBoxNumber.ToString());
            labelNumber.Visible = true;
            if (match.Success == true)
            {
                labelNumber.ForeColor = Color.Green;
                labelNumber.Text = "OK";                
            }
            else
            {
                labelNumber.ForeColor = Color.Red;
                labelNumber.Text = "Помилка";
            }
        }

        private void checkName_Click(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"[А-ЯІЇЄ][а-яіїє]+");
            Match match = regex.Match(textBoxName.ToString());
            labelName.Visible = true;
            if (match.Success == true)
            {
                labelName.ForeColor = Color.Green;
                labelName.Text = "OK";
            }
            else
            {
                labelName.ForeColor = Color.Red;
                labelName.Text = "Помилка";
            }
        }

        private void checkTime_Click(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"([01]\d|2[0-3]):([0-5]\d)");
            Match match = regex.Match(textBoxTime.ToString());
            labelTime.Visible = true;
            if (match.Success == true)
            {
                labelTime.ForeColor = Color.Green;
                labelTime.Text = "OK";
            }
            else
            {
                labelTime.ForeColor = Color.Red;
                labelTime.Text = "Помилка";
            }            
        }

        private void checkMail_Click(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"(\w+\.?\w+)@([a-z]+)\.([a-z]+)");
            Match match = regex.Match(textBoxMail.ToString());
            labelMail.Visible = true;
            if (match.Success == true)
            {
                labelMail.ForeColor = Color.Green;
                labelMail.Text = "OK";
            }
            else
            {
                labelMail.ForeColor = Color.Red;
                labelMail.Text = "Помилка";
            }
        }
    }
}
