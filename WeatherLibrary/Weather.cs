﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;

namespace WeatherLibrary
{
    public class Weather
    {
        protected string temperature;
        protected string wind;
        protected string windDirection;
        protected string humidity;
        protected string water;
        protected string text;
        protected string picture;      

        public Weather()
        {
            WebClient web = new WebClient();
            web.Encoding = Encoding.UTF8;

            string htmlcode = web.DownloadString("https://www.gismeteo.ua/ua/weather-zhytomyr-4943/");
            Regex rgx = new Regex("((?:(?:\\+|-)\\d\\d?)|0).+meas.+C|\\b(\\d).+м\\/с|Вітер ([а-яєїі]+(?:-[а-яєїі]+)?)|(\\d\\d?).+волог|(\\d\\d?).+C.+вода|png\" title=\"(.+)\" s.+\\((.+\\.png)\\)\"");
            MatchCollection matches = rgx.Matches(htmlcode);           
            
            temperature = matches[1].Groups[1].Value + " °C";
            wind = matches[3].Groups[2].Value + " м/с";
            windDirection = matches[2].Groups[3].Value;
            humidity = matches[4].Groups[4].Value + "%";
            water = matches[5].Groups[5].Value + " °C";
            text = matches[0].Groups[6].Value;
            picture = "https:" + matches[0].Groups[7].Value;
        }

        public string Temperature
        {
            set
            {
                temperature = value;
            }
            get
            {
                return temperature;
            }
        }

        public string Wind
        {
            set
            {
                wind = value;
            }
            get
            {
                return wind;
            }
        }

        public string WindDirection
        {
            set
            {
                windDirection = value;
            }
            get
            {
                return windDirection;
            }
        }

        public string Humidity
        {
            set
            {
                humidity = value;
            }
            get
            {
                return humidity;
            }
        }

        public string Water
        {
            set
            {
                water = value;
            }
            get
            {
                return water;
            }
        }

        public string Text
        {
            set
            {
                text = value;
            }
            get
            {
                return text;
            }
        }

        public string Picture
        {
            set
            {
                picture = value;
            }
            get
            {
                return picture;
            }
        }        
    }
}