﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeatherLibrary;

namespace WeatherForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            Weather weather = new Weather();            
            labelTemperature.Text = weather.Temperature;
            labelWind.Text = weather.Wind;
            labelWindDirection.Text = weather.WindDirection;
            labelHumidity.Text = weather.Humidity;
            labelWater.Text = weather.Water;
            labelText.Text = weather.Text;
            pictureBoxWeather.Text = weather.Picture;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Weather weather = new Weather();
            labelTemperature.Text = weather.Temperature;
            labelWind.Text = weather.Wind;
            labelWindDirection.Text = weather.WindDirection;
            labelHumidity.Text = weather.Humidity;
            labelWater.Text = weather.Water;
            labelText.Text = weather.Text;
            pictureBoxWeather.Text = weather.Picture;            
        }
    }
}